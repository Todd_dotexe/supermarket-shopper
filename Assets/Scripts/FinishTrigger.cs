﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTrigger : MonoBehaviour
{
    public GameObject victoryText;
    Timer timer;

    void Start()
    {
        victoryText.SetActive(false);
        timer = GameObject.Find("Timer").GetComponent<Timer>();
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
          if (other.gameObject.GetComponent<PlayerInventory>().numberofItems == 6)
            {
                victoryText.SetActive(true);
                timer.canRestart = true;
            }
        }
    }
}

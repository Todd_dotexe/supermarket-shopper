﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerInventory : MonoBehaviour
{
    public GameObject cam;
    public List<string> items = new List<string>();
    public Text inventoryText;
    public Text objectiveText;
    public int numberofItems = 0;

    private void Start()
    {
        cam = Camera.main.gameObject;
        inventoryText.text = string.Empty;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Physics.Raycast(cam.transform.position, cam.gameObject.transform.forward, out var hit, 4f))
            {
                if (hit.collider.GetComponent<ShopItem>())
                {
                    items.Add(hit.collider.GetComponent<ShopItem>().name);
                    Destroy(hit.collider.gameObject);
                    UpdateInventory();
                }
            }
        }
    }
    public void UpdateInventory()
    {
        numberofItems = numberofItems + 1;
        inventoryText.text = string.Empty;

        string inventory = string.Empty;
        foreach (string item in items)
        {
            inventory += item + "\n";
        }
        inventoryText.text = inventory;
    }
}

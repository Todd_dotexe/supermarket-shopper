﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayIdentify : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        float distanceofRay = 100;

        if (Physics.Raycast(Camera.main.transform.position,Camera.main.transform.forward, out hit, distanceofRay))
        {
            Debug.Log(hit.transform.name);
        }
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * distanceofRay);
    }
}

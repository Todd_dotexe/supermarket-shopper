﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{

    public float timeRemaining = 10;
    public bool timerIsRunning = false;
    public bool canRestart;

    //GUI Text
    public Text timeText;
    public GameObject gameOverText;
    public GameObject restartText;

    //Disables Text and Activates Timer
    void Start()
    {
        timerIsRunning = true;
        gameOverText.SetActive(false);
        restartText.SetActive(false);
    }

    //Updates every frame
    void Update()
    {
        //Timer is Running
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
        //Activates if Timer runs out
        else
        {
            gameOverText.SetActive(true);
            restartText.SetActive(true);
            timeRemaining = 0;
            timerIsRunning = false;
            canRestart = true;
        }

        DisplayTime(timeRemaining);

        if (Input.GetKeyDown(KeyCode.R) && canRestart == true)
        {
            RestartGame();
        }
    }

    //Restart Function
    public void RestartGame()
    {
        gameOverText.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    //Calculates time in Minutes and Seconds
    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
